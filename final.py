

from pythonosc import udp_client
from pythonosc import osc_message_builder

from pythonosc.dispatcher import Dispatcher
from pythonosc import osc_server



def getReply(unused_addr, args, query):
	print("{0} {1} \n".format(args[0], query))
	client = udp_client.SimpleUDPClient("127.0.0.1", 7400)
	print(client)
	
	client.send_message("/test", query)


if __name__ == "__main__":


	dispatcher = Dispatcher()
	dispatcher.map("/hello", getReply, "you said: ")

	server = osc_server.ThreadingOSCUDPServer(("127.0.0.1", 7500), dispatcher)
	print("Serving on {}".format(server.server_address))
	server.serve_forever()

	